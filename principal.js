const express = require('express');
const app = express();
const fs = require('fs');

const json_per= fs.readFileSync('./lista.json', 'utf-8');
let personas=JSON.parse(json_per);

app.listen(5000, () =>{
console.log('Server on port 5000');
});
//config
app.set('view engine', 'ejs');
//middleware
app.use(express.urlencoded({extended: false}));

app.get('/', (req, res)=>{
    res.render('index.ejs');
    personas
});

app.get('/factura', (req, res)=>{
res.render('factura.ejs',{
})
});

app.get('/datos', (req, res)=>{
    res.render('datos.ejs',{
        personas
    })
    });

 app.post('/', (req, res)=>{
const {usuario, persona, pais, provincia, canton} = req.body;
if (!usuario || !persona || !pais || !provincia || !canton) {
    res.status(400).send('Debe llenar todos los datos');
    return;
}
let newPer ={
usuario,
persona,
pais,
provincia,
pais
};     
personas.push(newPer);
const json_per = JSON.stringify(personas);
fs.writeFileSync('./lista.json', json_per, 'utf-8');
res.redirect('/');
 });

 app.get('/delete/:usuario', (req, res)=>{
personas = personas.filter(per=>per.usuario !=req.params.usuario);
const json_per = JSON.stringify(personas)
fs.writeFileSync('./lista.json', json_per, 'utf-8');
res.redirect('/');
 });